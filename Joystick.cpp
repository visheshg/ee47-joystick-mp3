/**
 * File: Joystick.cpp
 * Created: Vishesh Gupta 29 May 2013
 *
 *
 * Contains the implementations of the functions shown in Joystick.h
 */

#include "Joystick.h"

// constructor initializes all the pins.
Joystick::Joystick(const int joyup, const int joyright, const int joybutton) {
    this->joyup = joyup;
    this->joyright = joyright;
    this->joybutton = joybutton;
}

int Joystick::getJoyUp() const {
    return analogRead(this->joyup);
}

int Joystick::getJoyRight() const {
    return analogRead(this->joyright);
}

Joystick::joystick_pos Joystick::getPosition() const {
    int joyup = getJoyUp();
    int joyright = getJoyRight();
    // Top row
    if (joyup < ANALOG_MAX/3) {
        if (joyright < ANALOG_MAX/3) {
            return JOY_TOP_LFT;
        } else if (joyright < ANALOG_MAX/3 * 2) {
            return JOY_TOP_MID;
        } else {
            return JOY_TOP_RHT;
        }
    } else if (joyup < ANALOG_MAX/3 * 2) { // mid row
        if (joyright < ANALOG_MAX/3) {
            return JOY_MID_LFT;
        } else if (joyright < ANALOG_MAX/3 * 2) {
            return JOY_MID_MID;
        } else {
            return JOY_MID_RHT;
        }
    } else { // bottom row
        if (joyright < ANALOG_MAX/3) {
            return JOY_BOT_LFT;
        } else if (joyright < ANALOG_MAX/3 * 2) {
            return JOY_BOT_MID;
        } else {
            return JOY_BOT_RHT;
        }
    }
}




