/**
Created by: Vishesh Gupta 29 May 2013
This file contains the implementation of the Levenshtein distance algorithm which computes the 
"edit distance" between two strings. 
*/

#include "Levenshtein.h"

//const char* gestureDesc[] = {circle, up, down, left, right};


enum Gesture classifyGesture(const char* g) {
    int minDistance = levenshtein(g, gestureDesc[0], -1); 
    enum Gesture min = (Gesture) 0;
    for (int i = 1; i < NUM_GESTURES; i++) {
        int distance = levenshtein(g, gestureDesc[i], minDistance);
        if (distance < minDistance) {
            min = (enum Gesture) i;
            minDistance = distance; 
        }
    }
    return min;
}

/**
 * Computes the minimum of three numbers, possibly not the most efficient way, 
 * but a easily understandable way.
 * @param  one   the first number
 * @param  two   the second number
 * @param  three the third number
 * @return       the smallest number amongst the three. 
 */
int min3(int one, int two, int three) {
    int small = one < two ? one : two;
    int smallest = small < three ? small : three;
    return smallest;
}


/**
 * @see editDistance(const char*, const char*, int)
 * editDistanceH should not be called by any client, it is the internal recursive
 * helper function for computing edit distance.
 *
 * @param  word    the word starting point.
 * @param  target  the target word create by editing.
 * @param  max     the max number of allowed edits
 * @param  wordLen length of word
 * @param  targLen length of target - the lengths are optimizations which save a 
 *                 strlen(word) call in each recursive step.
 * @param  wordI   current index of word to compare from (should be 0 at call)
 * @param  targI   current index of target to compare from (should be 0 at call)
 * @param  depth   the current depth of recursive calls (should also be 0 at call)
 * 
 * @return         the minimum number of edits it takes to transform word into target.
 *                 alternately, max+1 if the max value was exceeded.
 */
int levenshteinH(const char* word, const char* target, int max,
                  int wordLen, int targLen,
                  int wordI, int targI, int depth) {

    // BASE CASES:
    if (max != -1 && depth > max) return depth;

    int wordL = wordLen-wordI;
    int targL = targLen-targI;
    
    if (wordL == 0 && targL == 0) {
        return depth;
    }
    // END BASE CASES
    
    // NO CHANGE CASE:
    // If the first characters match, then no change is needed and the recursion can
    // simply advance a character each.
    // 
    // note: this case can't trigger in the case that only one of wordL or targL are '\0'.
    // no need for explicit check.
    if (word[wordI] == target[targI]) {
        return levenshteinH(word, target, max, wordLen, targLen, wordI+1, targI+1, depth);
    }
    
    // edit distance must be bounded by the length of the longer word.
    // this could ALSO be optimized by passing it in as a parameter, but I really
    // don't feel like having 9 of them.
    int sentinnel = wordLen > targLen ? wordLen : targLen;

    // Explanation of recursive insight:
    // consider word = "dell" and target = "hello".
    // We can:
    // Replace a letter, in which case, compare remaining portions
    // of the word so word = "ell" and target  = "ello"
    // 
    // Add a letter, in which case, the target needs to be shifted one,
    // so word = "dell" and target = "ello"
    // 
    // Remove a letter, in which case, the word needs to be shifted one,
    // so word = "ell" and target = "hello". 
    // 
    // These are the three cases detailed below.


    // REPLACE CASE:
    // note, have to make sure that both strings have a valid character.
    int repD = sentinnel; 
    if (wordL != 0 && targL != 0) {
        repD = levenshteinH(word, target, max, wordLen, targLen, wordI+1, targI+1, depth+1);
    }

    // ADD CASE:
    int addD = sentinnel;
    if (targL != 0) {
        addD = levenshteinH(word, target, max, wordLen, targLen, wordI, targI+1, depth+1);
    }

    // REMOVE CASE:
    int remD = sentinnel;
    if (wordL != 0) {
        remD = levenshteinH(word, target, max, wordLen, targLen, wordI+1, targI, depth+1);
    }

    return min3(repD, addD, remD);

}

/**
 * Computes the edit distance between two words. An edit is one of:
 * the removal of a letter, addition of a letter, or replacement of a letter. 
 * The method will return a number up to the max passed in. To not have a max,
 * -1 will function as a sentinnel, and the edit distance will be computed as 
 * an actual correct value. The max parameter can be used to optimize (if you 
 * only happen to care that the edit distance be less than max). The maximum 
 * value possibly returned by this function is max+1 or the edit distance if 
 * max=-1. 
 * 
 * @param  word   the start word
 * @param  target the target to transform word into
 * @param  max    the max number of allowed recursions.
 * @return        the edit distance between word and target.
 *                alternately, max+1 if max was exceeded.
 */
int levenshtein(const char* word, const char* target, int max) {
  return levenshteinH(word, target, max, strlen(word), strlen(target), 0,0,0);
}

