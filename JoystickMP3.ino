/*
 * example sketch to play audio file(s) in a directory, using the VS1053 library
 * for playback and the arduino sd library to read files from a microsd card.
 * pins are setup to work well for Arduino Micro.
 * 
 * originally based on frank zhao's player: http://frank.circleofcurrent.com/
 * utilities adapted from previous versions of the functions by matthew seal.
 *
 * (c) 2011, 2012 david sirkin sirkin@cdr.stanford.edu
 *                akil srinivasan akils@stanford.edu
 *
 *
 * Vishesh Gupta 28 May 2013 
 * Changed a lot of this file to operate on a control by joystick
 * basis.
 * 
 */

#include <SPI.h>
#include <SD.h>
#include <EEPROM.h>
#include <VS1053.h>
#include <Adafruit_GFX.h>
#include <Adafruit_ST7735.h>
#include "Joystick.h"
#include "JoystickGestures.h"
#include "Levenshtein.h"

#define max_title_len 60
#define max_name_len  13
#define max_num_songs 40
#define read_buffer  256
#define sd_cs         17
// this may seem strange, but volume is set up so that 0 = loud and 255 = soft.
#define MAX_MP3_VOL   20
#define MIN_MP3_VOL   255
#define MAX_GESTURE_LEN 13 // 12 + \0

// volume
volatile int mp3_vol = 100;

// pin definitions for the joystick input.
const int JOYUP = A4;
const int JOYLEFT = A5;

VS1053 myVS(A0, A1, A2, -1);
Adafruit_ST7735 display = Adafruit_ST7735(10, 9, 8);
File sd_file;
unsigned char num_songs = 0, current_song = 0;
char fn[max_name_len];
char title[max_title_len + 1];
enum mp3_state { DIR_PLAY, MP3_PLAY, PAUSED };
volatile mp3_state current_state = PAUSED;

enum screen_state {SONGS_LIST, VOLUME};
volatile screen_state current_screen = SONGS_LIST;

volatile int open_song = current_song;

Joystick joy(JOYUP, JOYLEFT, -1); // joystick setup.
JoystickGestures gestures(&joy);
char gesture[MAX_GESTURE_LEN]; // stores the current joystick gesture.

void setup() {
  Serial.begin(9600);
  SPI.begin();
  myVS.begin();
  
  // init display
  display.initR(INITR_BLACKTAB); // corresponds to my ST7735
  display.setTextColor(ST7735_BLACK);
  display.setTextWrap(true);

  Serial.println("Hello!");
  delay(1000);
  display.fillScreen(ST7735_WHITE);
  display.setCursor(0,0);
  display.print("Barebones Mp3!");
  Serial.println("Barebones Mp3!");
  delay(2000);
  
  sd_card_setup();
  sd_dir_setup();
  
  writeFnsToDisplay();
  starCurrentSong();
  openSongFile(current_song);
  myVS.setVolume(mp3_vol);
}

/** writes the filename of each song to the display and refreshes the display.
*   Used as the setup from the SD card to then choose which
*   song to play with the joystick
*/
void writeFnsToDisplay() {
  display.fillScreen(ST7735_WHITE);
  display.setTextWrap(false);
  display.setCursor(0,0);
  display.print("SONGS: \n\n");
  for (int i = 0; i < num_songs; i++) {
    getSongFilename(i);
    display.println(fn);
  }
}

void starCurrentSong() {
  Serial.println("starCurrentSong()");
  getSongFilename(current_song);
  display.setCursor(0, 8*(current_song+2));
  display.setTextColor(ST7735_BLACK, ST7735_WHITE);
  display.print("* ");
  display.print(fn);
}

void unStarCurrentSong() {
  Serial.println("unStarCurrentSong()");
  getSongFilename(current_song);
  display.setCursor(0, 8*(current_song+2));
  display.setTextColor(ST7735_BLACK, ST7735_WHITE);
  display.print(fn);
  display.print("  ");
}

void dir_play() {
  if (sd_file) {
    mp3_play();
  }
  else {
    // since 'sd_file' isn't open, the recently playing song must have ended.
    // increment the index, and open the next song, unless it's the last song
    // in the directory. in that case, just set the mp3_state to PAUSED.
    if (current_song < (num_songs - 1)) {
      current_song++;
      openSongFile(current_song);
      myVS.startSong();
    }
    else {
      current_state = PAUSED;
    }
  }
}  

void mp3_play() {
  unsigned char bytes[read_buffer]; // buffer to read and send to the decoder
  unsigned int bytes_to_read;       // number of bytes read from microsd card

  // first fill the 'bytes' buffer with (up to) 'read_buffer' count of bytes.
  // that happens through the 'sd_file.read()' call, which returns the actual
  // number of bytes that were read (which can be fewer than 'read_buffer' if
  // at the end of the file). then send the retrieved bytes out to be played.
  
  // 'sd_file.read()' manages the index pointer into the file and knows where
  // to start reading the next batch of bytes. 'Mp3.play()' manages the index
  // pointer into the 'bytes' buffer and knows how to send it to the decoder.

  bytes_to_read = sd_file.read(bytes, read_buffer);
  myVS.playChunk(bytes, bytes_to_read);
  
  // 'bytes_to_read' is only smaller than 'read_buffer' when the song's over.

  if (bytes_to_read < read_buffer) {
    sd_file.close();
    myVS.stopSong();
    
    // if we've been in the MP3_PLAY mp3_state, then we want to pause the player.
    if (current_state == MP3_PLAY) {
      current_state == PAUSED;
    }
  }
}

void doGesture(enum Gesture g) {
  switch(g){
    case UP_DOWN:
      display.setCursor(0,0);
      display.print("                     ");
      display.setCursor(0,0);
      if (current_screen == VOLUME) {
        current_screen = SONGS_LIST;
        display.print("SONGS:");
      }
      else {
        current_screen = VOLUME;
        display.print("VOLUME MODE ENGAGED");
      }
      break;
    case UP:
      if (current_screen == VOLUME) {
        mp3_vol--; if (mp3_vol < 20) mp3_vol = 20;
        myVS.setVolume(mp3_vol);
        break;
      }
      if (current_song == 0) break;
      unStarCurrentSong();
      current_song--;
      Serial.println(current_song);
      starCurrentSong();
      break;
    case DOWN:
      if (current_screen == VOLUME) {
        mp3_vol++; if (mp3_vol > MIN_MP3_VOL) mp3_vol = MIN_MP3_VOL;
        myVS.setVolume(mp3_vol);
        break;
      }
      if (current_song >= num_songs-1) break;
      unStarCurrentSong();
      current_song++;
      Serial.println(current_song);
      starCurrentSong();
      break;      
    case RIGHT:
      if (open_song != current_song) {
        openSongFile(current_song);
        open_song = current_song;
      }
      Serial.println("Starting song");
      current_state = DIR_PLAY;
      break;
    case LEFT:
      Serial.println("Stopping song");
      current_state = PAUSED;
      break;
    }
}

void loop() {
  gestures.recordPosition();
  if (gestures.detectedGesture()) {
    Serial.print("Gesture detected!: "); 
    Serial.println(gestures.getGesture());
    Serial.println("");
    enum Gesture g = classifyGesture(gestures.getGesture());
    Serial.print("Gesture classified as: ");
    Serial.println(gestureDesc[(int)g]);
    
    doGesture(g);
    
    
    gestures.clearGesture();
  }

  switch(current_state) {

    case DIR_PLAY:
      dir_play();
      break;

    case MP3_PLAY:
      mp3_play();
      break;

    case PAUSED:
      break;
  }
}











