/**
 * File: Joystick.h
 * Created: Vishesh Gupta on 29 May 2013
 * 
 * Contains the definition of a joystick object. 
 * 
 * The presumed joystick orientation is that the upper left corner of the 
 * joystick is (0,0) and the bottom right is (1023,1023), just like a 
 * computer screen. 
 *
 * https://www.sparkfun.com/products/9032 is the type of joystick this is
 * modeled after (a simple Thumb Joystick similar to the ones in the PS2).
 *
 *
 * Each number references a position of the joystick, as defined below
 * ------------------- DIAGRAM ---------------------
 *                      1 2 3
 *                      8 0 4
 *                      7 6 5
 * ----------------- END DIAGRAM -------------------
 *
 * Where 0 is the joystick neutral position.
 * The position requires some knowledge of the orientation of the joystick.
 * For the URL shown, the joystick should be oriented so that the button faces
 * to the right (cardinal east) with respect to the main knob.
 *
 * That way, the reading (0,0) maps to the top left, i.e., 1, and the reading
 * (1023,1023) corresponds to "5" which is also the bottom right.
 *
 * This position mapping is the basis for the gesture recognition library
 * for the joystick. Some of the gestures are orientation independent (circle,
 * for example) but the naming convention is based on the diagram shown above.
 * 
 */

#ifndef Joystick_h
#define Joystick_h

#include "Arduino.h"

class Joystick {
private:    

// FIELDS
    
    // pins that correspond to joystick up and joystick left pots
    // pin that corresponsds to the input from the joystick button.
    int joyup, joyright, joybutton;


// PRIVATE CONSTANTS and other definitions

    static const int ANALOG_MAX = 1023;

    // LFT = left RHT = right
    enum joystick_pos {
    JOY_TOP_LFT = 1, JOY_TOP_MID = 2, JOY_TOP_RHT = 3, 
    JOY_MID_LFT = 8, JOY_MID_MID = 0, JOY_MID_RHT = 4, 
    JOY_BOT_LFT = 7, JOY_BOT_MID = 6, JOY_BOT_RHT = 5 };

public:
    /**
     * Creates a new Joystick with the information about where each of the
     * pins are going. Note that joyup/joyright MUST be defined, but joybutton
     * doesn't have to be. Pass -1 for joybutton if it isn't connected.
     * 
     * @param joyup     the up-down axis pot analog pin
     * @param joyright   the left-right axis pot analog pin
     * @param joybutton the button's digital pin, or -1 if no button
     * 
     */
    Joystick(const int joyup, const int joyright, const int joybutton);
    ~Joystick() {} // there's nothing to destroy

    /**
     * Polls the joystick for the value of the up-down axis pin.
     * Note, this is orientation insensitive (just an analog read).
     * @return a number from 0-1023 of the analog read.
     */
    int getJoyUp() const;

    /**
     * Polls the joystick for the value of the left-right axis pin.
     * Note, this is orientation insensitive (just an analog read).
     * @return a number from 0-1023 of the analog read.
     */
    int getJoyRight() const;
    
    /**
     * Returns the position of the joystick as a number from 0-8
     * based on the readings of the pots. The presumed orientation
     * is that up and left is (0,0) readings on both pots, hence the
     * naming convention of the variables. 
     * ---------------- DIAGRAM ------------------------
     *                      1 2 3
     *                      8 0 4
     *                      7 6 5
     * ----------------- END DIAGRAM -------------------
     *
     *
     * Threshold information: Based on tests of the joystick, I've noticed
     * that the base seems to operate in a square fashion (but somehow feels
     * like you're moving in a circle) so all I've done is broken up the square
     * into a 3x3 grid and mapped each grid to its appropriate number. 
     * This is probably not perfect, and I might need to make it a circle 
     * threshold instead of a jagged rectangular one. 
     * 
     * 
     * @return         a number from 0-8 corresponding to a position in the 
     *                 diagram shown in the documentation for this file.
     */
     joystick_pos getPosition() const;
};




#endif










