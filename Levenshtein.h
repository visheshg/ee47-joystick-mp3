/**
 * File: Levenshtein.h
 *
 * Interface for the Levenshtein c file.
 */

#ifndef Levenshtein_h
#define Levenshtein_h

#include "Arduino.h"

// yes, there are more than this defined below, the constant reflects how 
// many gesture I really need right now.
#define NUM_GESTURES 5

enum Gesture {
    UP,
    DOWN,
    LEFT,
    RIGHT,
    UP_DOWN,
    DOWN_UP,
    LEFT_RIGHT,
    RIGHT_LEFT,
    CIRCLE
};

const char* const gestureDesc[] = {
    "20", "60", "80", "40", "2060", "6020", "8040", "4080", "123456780"};

/**
 * Computes the edit distance between two words. An edit is one of:
 * the removal of a letter, addition of a letter, or replacement of a letter. 
 * The method will return a number up to the max passed in. To not have a max,
 * -1 will function as a sentinnel, and the edit distance will be computed as 
 * an actual correct value. The max parameter can be used to optimize (if you 
 * only happen to care that the edit distance be less than max). The maximum 
 * value possibly returned by this function is max+1 or the edit distance if 
 * max=-1. 
 * 
 * @param  word   the start word
 * @param  target the target to transform word into
 * @param  max    the max number of allowed recursions.
 * @return        the edit distance between word and target.
 *                alternately, max+1 if max was exceeded.
 */
int levenshtein(const char* word, const char* target, int max);

/**
 * Computes the minimum of three numbers, possibly not the most efficient way, 
 * but a easily understandable way.
 * @param  one   the first number
 * @param  two   the second number
 * @param  three the third number
 * @return       the smallest number amongst the three. 
 */
int min3(int one, int two, int three);

/**
 * returns something of type Gesture with the best possible classification
 * of the string g.
 * ie. "2" would return UP. "23" would return UP, but "24" could
 * return UP or RIGHT.
 * 
 * @param  g a string eg "12345678" representing a gesture. see JoystickGesture
 * @return   something of type Gesture, or NONE if the gesture was undefined.
 */
enum Gesture classifyGesture(const char* g);


#endif
