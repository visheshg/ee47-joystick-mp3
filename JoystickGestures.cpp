/**
 * File: JoystickGestures.cpp
 * Created: Vishesh Gupta 30 May 2013
 *
 * Contains the implementation for the JoystickGestures class.
 * 
 */

#include "JoystickGestures.h"

#define DEBUG false

JoystickGestures::JoystickGestures(Joystick* joy) {
    this->joystick = joy;
    this->cursor = 0;
    this->gesture[0] = '\0';
    this->gesture[GESTURE_MAX] = '\0'; // failsafe, not really required.
    this->gestureComplete = false;
    this->prevJoyUp = joy->getJoyUp();
    this->prevJoyRight = joy->getJoyRight();
}

void JoystickGestures::recordPosition() {
    // if we've already detected a gesture, then just return
    // since we need to wait for clearGesture() before we read another gesture.
    if (detectedGesture()) return;

    int pos = this->joystick->getPosition();
    int joyup = this->joystick->getJoyUp();
    int joyright = this->joystick->getJoyRight();

    // velocities
    int dUp = joyup - this->prevJoyUp;
    int dRight = joyright - this->prevJoyRight;
    // set the prevs
    this->prevJoyUp = joyup;
    this->prevJoyRight = joyright;

    if (DEBUG && (dUp > 1 || dUp < -1) && (dRight > 1 || dRight < -1)) {
        Serial.print("velocities: "); Serial.print(dUp);
        Serial.print(" "); Serial.println(dRight);
    }


    char position = '0'+ pos;
    if (position == '0') {
        // "idle" situation - user is not doing anything, joystick neutral
        if (cursor == 0) return;
        if (dUp == 0 && dRight == 0) {
            // stationary joystick indicates that the gesture is done
            this->gestureComplete = true;
            return;
        }
    }
    // note we don't want to take repeated polls of the same number  
    // (the second case there)  
    if (cursor == 0 || gesture[cursor-1] != position) {
        
        if (DEBUG) {
            Serial.print("recordPosition() - position is "); 
            Serial.println(position);
            Serial.print("joystick pos is "); Serial.println(pos);
        }
        gesture[cursor] = position;
        if (DEBUG) {
            Serial.print("gesture["); Serial.print(cursor);
            Serial.print("] = "); Serial.println(gesture[cursor]);
        }

        gesture[cursor+1] = '\0';
        cursor++;
        if (DEBUG) {
            for (int i = 0; i < GESTURE_MAX && gesture[i] != '\0'; i++) {
              Serial.print(gesture[i]);
            }
            Serial.println("");
        }

        // cap gesture length.
        if(cursor == GESTURE_MAX) this->gestureComplete = true;  
    }
}

void JoystickGestures::clearGesture() {
    // reset this object so it looks like it was just constructed.m
    this->cursor = 0;
    this->gesture[0] = '\0';
    this->gestureComplete = false;
}





























