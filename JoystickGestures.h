/**
 * File: JoystickGestures.h 
 * Created: Vishesh Gupta 29 May 2013
 * 
 *
 * Contains the description of a class designed to poll the Joystick 
 * repeatedly and wait for a gesture to be recognized.
 * 
 * Basically, call poll() each time through the loop of a function to get 
 * the next joystick movement.
 *
 * A "gesture" is defined as a movement away from 0 to some other numbers
 * then back to 0 again. (*** this may change after testing.)
 *
 * When a gesture is detected detectedGesture() will return true and 
 * getGesture() will return a pointer to the completed gesture. 
 *
 * Gestures are constrained to be a length specified in this header file.
 * The idea is that any user can easily change it, but at the cost of space.
 *
 * Also, gestures cannot have repeated values in them, i.e. "44" is not a 
 * valid gesture. Gestures by definition involve fluid movement, not sitting 
 * in one spot. The class only registers changes in the joystick position.
 *
 * 
 */


#ifndef JoystickGestures_h
#define JoystickGestures_h

#include "Arduino.h"
#include <map>
#include "Joystick.h"

#define GESTURE_MAX 12 // allows 12 positions in the gesture.


class JoystickGestures {
private:
    Joystick* joystick; // a pointer to a valid joystick object.
    char gesture[GESTURE_MAX+1]; // the array containing the gesture
    bool gestureComplete; // set to true when 0-something-0 pattern complete
    int cursor; // the index at which to place the next sensed position
    // used to calculate joystick "velocity"
    int prevJoyUp; // previous value of joyup
    int prevJoyRight; // previous value of joyright.

public:
    JoystickGestures(Joystick* joy);
    ~JoystickGestures() {}

    /**
     * Records the current position of the joystick and adds it to the end
     * of the joystick if applicable and sets all relevant variables. 
     */
    void recordPosition();

    /**
     * Resets the gesture (indicating that the previous detected gesture has
     * been read by some client program) and prepares for the next gesture.
     *
     * Does not check for detectedGesture, since you might want to clear
     * irrelevant of whether or not a gesture was detected.
     */
    void clearGesture();

    /**
     * Returns true if there's been a detected gesture, ie the joystick 
     * moved from 0 through some pattern back to 0.
     * @return true or false dependending on whether the gesture was detected.
     */
    bool detectedGesture() {
        return gestureComplete;        
    }

    /**
     * Returns a pointer to the current gesture as it stands.
     * Note, does not ensure detectedGesture first, just returns the pointer.
     * @return a pointer to the gesture as it's being filled
     */
    char* getGesture() {
        return gesture;
    }

    /**
     * See getGesture, the difference is that the pointer is only returned
     * if the gesture is complete. 
     * @return a pointer to the completed gesture, or NULL if the gesture is 
     * not complete.
     */
    char* getCompletedGesture() {
        if (detectedGesture()) return getGesture();
        else return NULL;
    }
};

#endif












